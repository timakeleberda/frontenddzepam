let contacts = [
  {
    name: "Tom",

    phoneNumber: "098-76-54-352"
  },

  {
    name: "Peter",

    phoneNumber: "098-54-54-652"
  },

  {
    name: "Ann",

    phoneNumber: "050-711-21-21"
  }
];
contacts.sort(function (a, b) {
  if (a.name > b.name) {
    return 1;
  }
  if (a.name < b.name) {
    return -1;
  }
  return 0;
});
contacts.forEach(i => console.log(i.name));
